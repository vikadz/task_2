import('@playwright/test').PlaywrightTestConfig;

Feature('Go on Search Page Google');

Scenario('Visible search result must be 10', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.fillField({ xpath: "//input[@name='q']" }, "javascript");
    I.click("Поиск в Google");
    let url = await I.grabCurrentUrl();
    I.amOnPage(url, 5);

    I.seeInField("//input[@class='gLFyf gsfi']", "javascript");
    I.seeNumberOfElements("//div[@class='tF2Cxc']", 10);
});

Scenario.only('Visible block: Top menu', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.fillField({ xpath: "//input[@name='q']" }, "javascript");
    I.click("Поиск в Google");
    let url = await I.grabCurrentUrl();
    I.amOnPage(url, 5);

    I.see("Все", "div.MUFPAc > div.hdtb-mitem.hdtb-msel:nth-child(1)");
    I.see("Картинки", "div.hdtb-mitem:nth-child(2) > a:nth-child(1)");
    I.see("Видео", "div.hdtb-mitem:nth-child(3) > a:nth-child(1)");
    I.see("Новости", "div.hdtb-mitem:nth-child(4) > a:nth-child(1)");
    I.see("Книги", "div.hdtb-mitem:nth-child(5) > a:nth-child(1)");

    I.see("Ещё", "div.GOE98c");
    I.click("div.GOE98c")
    I.see("Карты", "//a[contains(@href,'https://maps.google.com/maps')]", 3)
    I.see("Авиабилеты", "//a[contains(@href,'https://www.google.com/flights')]")
    I.see("Финансы", "//a[contains(@href,'https://www.google.com/finance')][contains(text(),'Финансы')]")
    I.see("Инструменты", "//div[@id='hdtb-tls']")
});