const { I } = inject();

module.exports = {
  page: "https://www.onliner.by/",

  topSecondaryMenu: {
    // insert your locators and methods here
    secondaryMenu: "//ul[@class='project-navigation__list project-navigation__list_secondary']",
    veloBake: "ul.project-navigation__list.project-navigation__list_secondary >:first-child",
    carTires: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(2)",
    engineOils: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(3)",
    noteBooks: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(4)",
    coffee: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(5)",
    speakers: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(6)",
    gardenTools: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(7)",
    greenhousesGreenhouses: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(8)",
    ladders: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(9)",
    wirelessRouters: "ul.project-navigation__list.project-navigation__list_secondary >:nth-child(10)"
  },

  clickItem(item) {
    I.click(item)
  }
}


