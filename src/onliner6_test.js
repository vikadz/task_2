Feature('Onliner - main page. Availability, clicking elementes of top menu and go on element page.');


Scenario('Input in search product Huawei, select and go on product page', async ({ I }) => {

    I.amOnPage('https://www.onliner.by/');

    I.seeElement("//div[@id='fast-search']//input[@class='fast-search__input']");
    I.wait(5);
    I.fillField("//div[@id='fast-search']//input[@class='fast-search__input']", "Huawei");
    I.wait(5);

    I.seeElement("//div[@id='fast-search-modal']");
    let pin = await I.grabTextFromAll("//body/div[@id='fast-search-modal']/div[1]/div[1]/iframe[1]");

    I.click("//body/div[@id='fast-search-modal']/div[1]/div[1]/iframe[1]");
    let url = await I.grabCurrentUrl();
    I.waitInUrl(url)
    I.seeElement("//h1");
    I.see(pin, "//h1");
});

