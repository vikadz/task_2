Feature('Onliner - main page. Availability, clicking elementes of top menu and go on element page.');

Scenario('Select in checkbox discount 15%, xiaomi, check appear button and delete it. ', ({ I }) => {

    I.amOnPage('https://www.onliner.by/');

    I.seeElement("//div[contains(text(),'Все суперцены')]");
    I.click("//div[contains(text(),'Все суперцены')]");
    I.waitInUrl("https://catalog.onliner.by/superprice");

    I.wait(10);

    I.seeElement("//span[contains(text(),'до 15%')]/../span[@class='i-checkbox']");
    I.click("//span[contains(text(),'до 15%')]/../span[@class='i-checkbox']");
    I.seeCheckboxIsChecked("//span[contains(text(),'до 15%')]/../span[@class='i-checkbox']");

    I.seeElement("//input[@value='xiaomi']/following-sibling::span");
    I.click("//input[@value='xiaomi']/following-sibling::span");
    I.seeCheckboxIsChecked("//input[@value='xiaomi']/following-sibling::span");

    I.seeElement("//span[@class='schema-filter-button__sub schema-filter-button__sub_control']");
    I.click("//span[@class='schema-filter-button__sub schema-filter-button__sub_control']");

    I.seeElement("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'до 15')]");
    I.click("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'до 15')]");
    I.dontSee("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'до 15')]");
    I.dontSeeCheckboxIsChecked("//span[contains(text(),'до 15%')]/../span[@class='i-checkbox']");

    I.seeElement("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'Xiaomi')]");
    I.click("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'Xiaomi')]");
    I.dontSee("//div[@id='schema-tags']//span[@class='schema-tags__text'][contains(text(),'Xiaomi')]")
    I.dontSeeCheckboxIsChecked("//input[@value='xiaomi']/following-sibling::span");
});