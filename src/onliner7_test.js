const { page, topSecondaryMenu } = require("../pages/onliner");


Feature('Onliner - main page. Availability, clicking elementes of top menu.');

Scenario('Elements of Main navigate window', async ({ I, onlinerPage }) => {
    I.amOnPage(page);

    I.seeElement(topSecondaryMenu.secondaryMenu);
    I.seeElement(topSecondaryMenu.veloBake);
    I.seeElement(topSecondaryMenu.carTires);
    I.seeElement(topSecondaryMenu.engineOils);
    I.seeElement(topSecondaryMenu.noteBooks);
    I.seeElement(topSecondaryMenu.coffee);
    I.seeElement(topSecondaryMenu.speakers);
    I.seeElement(topSecondaryMenu.gardenTools);
    I.seeElement(topSecondaryMenu.greenhousesGreenhouses);
    I.seeElement(topSecondaryMenu.ladders);
    I.seeElement(topSecondaryMenu.wirelessRouters); 
 
    onlinerPage.clickItem(topSecondaryMenu.ladders);
    let url = await I.grabCurrentUrl();
    I.waitInUrl(url, 3);

});