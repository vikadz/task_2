Feature('Onliner - main page. Availability, clicking elementes of top menu and go on element page.');

Scenario('Buttons: Catalog, Superprice', ({ I }) => {
    I.amOnPage('https://www.onliner.by/');
    I.seeElement("//a[@href='https://catalog.onliner.by/']");
    I.click("//a[@href='https://catalog.onliner.by/']");
    I.waitInUrl("https://catalog.onliner.by/", 5);
    I.seeElement("//span[contains(@class,'b-main-navig')][contains(text(),'Ката')]");
    I.seeElement("//a[contains(@class,'b-main-')][@href='https://catalog.onliner.by/superprice']");
});

Scenario('Select Autoflea market. Go on NewCars.', ({ I }) => {
    I.amOnPage('https://www.onliner.by/');

    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Автобарахолка')]");
    I.moveCursorTo("//span[contains(@class,'b-main-')][contains(text(),'Автобарахолка')]");
    I.waitForVisible("//li[@class='b-main-navigation__item b-main-navigation__item_arrow b-main-navigation__item_active']", 3);
    I.seeElement("//a[@href='https://ab.onliner.by/new-cars']");

    I.click("//a[@href='https://ab.onliner.by/new-cars']");
    I.waitInUrl("https://ab.onliner.by/new-cars", 5);
});