import('@playwright/test').PlaywrightTestConfig;

Feature('Page GoogleDoodles: ');

Scenario('Entering into the search bar text: Belarus. Go on page.', async ({ I }) => {
    I.amOnPage('https://www.google.com/doodles');
    I.seeElement("//input[@id='searchinput']");
    I.fillField("//input[@id='searchinput']", "Belarus")
    I.click("#searchbtn")
    let url = await I.grabCurrentUrl();
    I.amOnPage(url, 3);
});

Scenario('Entering into the search bar text: 2022 год. Go on page.', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.fillField({ xpath: "//input[@name='q']" }, "2022 год");
    I.click("Поиск в Google");
});
