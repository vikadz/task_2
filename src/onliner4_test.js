Feature('Onliner - main page. Availability, clicking elementes of top menu.');

Scenario('Elements of Main navigate window', ({ I }) => {
    I.amOnPage('https://www.onliner.by/');
    I.seeElement("//span[contains(@class,'b-main-navig')][contains(text(),'Ката')]");;
    I.seeInTitle('Onliner');

    I.seeElement("//a[contains(@class,'b-main-')][@href='https://catalog.onliner.by/superprice']")

    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Новости')]")
    I.moveCursorTo("//span[contains(@class,'b-main-')][contains(text(),'Новости')]");
    I.waitForVisible("//div[@class='b-main-navigation__dropdown b-main-navigation__dropdown_visible']", 3);
    I.seeElement("//a[contains(text(),'Кошелек')]");

    I.seeElement("//a[@href='https://auto.onliner.by/2022/01/31/razygryvaem-16-pylesosov-xiaomi-za-otzyvy-na-avto'][contains(@class,'b-main-')]")

    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Автобарахолка')]");
    I.moveCursorTo("//span[contains(@class,'b-main-')][contains(text(),'Автобарахолка')]");
    I.waitForVisible("//li[@class='b-main-navigation__item b-main-navigation__item_arrow b-main-navigation__item_active']", 3);

    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Дома и квартиры')]");
    I.moveCursorTo("//span[contains(@class,'b-main-')][contains(text(),'Дома и квартиры')]");
    I.waitForVisible("//li[@class='b-main-navigation__item b-main-navigation__item_arrow b-main-navigation__item_active']", 3);

    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Услуги')]");
    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Барахолка')]");
    I.seeElement("//span[contains(@class,'b-main-')][contains(text(),'Форум')]");
});