import('@playwright/test').PlaywrightTestConfig;

Feature('Go on Search Page Google');

Scenario.only('Visible block: Video. Must have must have three visible elements: name, video, link, label, key points', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.fillField({ xpath: "//input[@name='q']" }, "javascript");
    I.click("Поиск в Google");

    let url = await I.grabCurrentUrl();
    I.amOnPage(url, 5);

    I.seeInField("//input[@class='gLFyf gsfi']", "javascript");

    I.seeElement("//h3[@class='GmE3X'][contains(text(),'Видео')]")
    I.waitForVisible("//h3[@class='GmE3X'][contains(text(),'Видео')]")
    I.see("Видео", "//h3[@class='GmE3X'][contains(text(),'Видео')]")

    I.seeNumberOfElements("//cite[contains(text(),'YouTube')]", 3);
    I.seeNumberOfElements("//a[@class='X5OiLe'][contains(@href,'https://www.youtube.com/')]", 3);
    I.seeNumberOfElements("//div[contains(@class,'fc9yUc')]", 3);
    I.seeNumberOfElements("//div[contains(@class,'NqpkQc')]", 3)
    I.seeNumberOfElements("//div[@class='S1j8wb']", 3)

    const eachElement = codeceptjs.container.plugins('eachElement');
    await eachElement('tick all elem', "//span[@jsname='Q8Kwad']", async (el) => {
        await el.click();
    })
});

Scenario('Visible block: Related queries. Clickable.', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.fillField({ xpath: "//input[@name='q']" }, "2022 год");
    I.click("Поиск в Google");

    let url = await I.grabCurrentUrl();
    I.amOnPage(url, 5);

    I.seeInField("//input[@class='gLFyf gsfi']", "2022 год");

    I.seeElement("//span[@class='q8U8x'][contains(text(),'Похожие запросы')]")
    I.see("Похожие запросы", "//span[@class='q8U8x'][contains(text(),'Похожие запросы')]")

    const eachElement = codeceptjs.container.plugins('eachElement');
    await eachElement('tick all elem', "//div[@jsname='Q8Kwad']", async (el) => {
        await el.click();
    })
});

