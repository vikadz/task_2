Feature('Page GoogleDoodles: select Language of page');

Scenario('Select bulgarian and english languages', async ({ I }) => {
    I.amOnPage('https://www.google.com/doodles');
    I.seeElement("//li[@id='language-menu']");

    I.selectOption("//select[@id='lang-chooser']", "bg")
    const urlBg = await I.grabCurrentUrl();
    I.waitInUrl(urlBg, 3);

    I.seeElement("//li[@id='language-menu']");
    I.selectOption("//select[@id='lang-chooser']", "en")

    let urlEn = await I.grabCurrentUrl();
    I.waitInUrl(urlEn, 3);
});

Scenario('Select all languages', async ({ I }) => {
    I.amOnPage('https://www.google.com/doodles');

    const lang = await I.grabValueFromAll(locate('select').find('option'));
    lang.forEach(element => {
        I.selectOption('select', element)
    })
});