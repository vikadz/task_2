const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

exports.config = {
  tests: './src/*_test.js',
  output: './output',
  timeout: 15000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 10000
  },
  helpers: {
    Playwright: {
      // url: 'https://www.google.com/doodles',
      show: true,
      browser: 'chromium',
      // waitForNavigation: "networkidle0",
      trace : true
    },
    ChaiWrapper: {
      require: 'codeceptjs-chai'
     }
  },
  plugins: {
    allure: {
      enabled: true
  },
  stepTimeout: {
    enabled: true
 }
},
  include: {
    I: './steps_file.js',
    onlinerPage: './pages/onliner.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'codeceptjsPlaywright'
}
