Feature('Google-main page.');

Scenario('The presence of buttons: Im lucky, Google search, Search bar (input), Search magnifier', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.see("Google");
    I.seeElement("//div[contains(@class,'FPdoLc')]//input[@name='btnI']");
    I.seeElement("//div[@class='FPdoLc lJ9FBc']//input[@class='gNO89b']");
    I.seeElement("//input[@name='q']");
    I.seeElement("//div[contains(@class,'ibl')]");
});

Scenario('Button Im lucky clickable. Go to page GoogleDoodles. See logo-element', async ({ I }) => {
    I.amOnPage('https://www.google.com');
    I.see("Google");
    I.seeElement("//div[contains(@class,'FPdoLc')]//input[@name='btnI']");
    I.click("Мне повезёт!");

    let url = await I.grabCurrentUrl();
    I.waitInUrl(url, 3);
    I.waitForVisible("//a[@id='logo']");
});